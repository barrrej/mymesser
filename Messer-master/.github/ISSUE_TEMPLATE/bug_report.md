---
name: Bug report
about: Create a report to help us improve

---

### Description

[Description of the issue]

### Steps to Reproduce

1. [First Step]
2. [Second Step]
3. [and so on...]

**Expected behavior:** [What you expect to happen]

**Actual behavior:** [What actually happens]

### Versions

You can get this information from copy and pasting the output of `node -v` and `npm list | grep messer` from the command line. Also, please include the OS and what version of the OS you're running.

### Additional Information

Any additional information, configuration or data that might be necessary to reproduce the issue.
